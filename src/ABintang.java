
import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author yehezkiel
 */
public class ABintang {

    PriorityQueue<Board> antri = new PriorityQueue<Board>();
    HashMap<String, String> explore = new HashMap<String, String>();
    Board initial;
    boolean stat = false;
    int z = 1;
    Stack result = new Stack();

    public ABintang(String first) {
        int beban = getBeban(first);
        this.initial = new Board(first, beban);
    }

    public int getBeban(String temp) {
        String x = temp;
        int res = 0;
        for (int i = 0; i < x.length(); i++) {
            int z = (int)x.charAt(i)-'0';
            if (z != (i+1)) {
                res++;
            }
        }
        return res;
    }

    public void isComplete(Board res) {
//        System.out.println(z);
//        System.out.println(res.initial);
//        System.out.println("");
//        z++;
        if (res.initial.equals("123456789")) {
            String temp = res.initial;
            while (!temp.equals(initial.initial)) {
                result.add(temp);
                temp = explore.get(temp);
            }
            System.out.println("Solution found in " + this.result.size() + " iteration");
            printResult();
            stat = true;
        }
    }

    public void checkFinal() {
        Board res = null;
        while (!stat) {
            if (antri.isEmpty()) {
                isComplete(initial);
                kiri(initial);
                kanan(initial);
                atas(initial);
                bawah(initial);
            } else {
                res = antri.remove();
                kiri(res);
                kanan(res);
                atas(res);
                bawah(res);
            }
        }
    }

    public void kiri(Board x) {
        int y = x.initial.indexOf("9");
        if (y != 0 && y != 3 && y != 6) {
            StringBuilder sb = new StringBuilder(x.initial);
            char temp = x.initial.charAt(y - 1);
            sb.setCharAt(y, temp);
            sb.setCharAt(y - 1, x.initial.charAt(y));
            Board newBoard = new Board(sb.toString(), getBeban(sb.toString()));
            if (!explore.containsKey(sb.toString())) {
                explore.put(sb.toString(), x.initial);
                isComplete(newBoard);
                antri.add(newBoard);
            }
        }

    }

    public void kanan(Board x) {
        int y = x.initial.indexOf("9");
        if (y != 2 && y != 5 && y != 8) {
            StringBuilder sb = new StringBuilder(x.initial);
            char temp = x.initial.charAt(y + 1);
            sb.setCharAt(y, temp);
            sb.setCharAt(y + 1, x.initial.charAt(y));
            Board newBoard = new Board(sb.toString(), getBeban(sb.toString()));
            if (!explore.containsKey(sb.toString())) {
                explore.put(sb.toString(), x.initial);
                isComplete(newBoard);
                antri.add(newBoard);
            }
        }
    }

    public void atas(Board x) {
        int y = x.initial.indexOf("9");
        if (y > 2) {
            StringBuilder sb = new StringBuilder(x.initial);
            char temp = x.initial.charAt(y - 3);
            sb.setCharAt(y, temp);
            sb.setCharAt(y - 3, x.initial.charAt(y));
            Board newBoard = new Board(sb.toString(), getBeban(sb.toString()));
            if (!explore.containsKey(sb.toString())) {
                explore.put(sb.toString(), x.initial);
                isComplete(newBoard);
                antri.add(newBoard);
            }
        }
    }

    public void bawah(Board x) {
        int y = x.initial.indexOf("9");
        if (y < 6) {
            StringBuilder sb = new StringBuilder(x.initial);
            char temp = x.initial.charAt(y + 3);
            sb.setCharAt(y, temp);
            sb.setCharAt(y + 3, x.initial.charAt(y));
            Board newBoard = new Board(sb.toString(), getBeban(sb.toString()));
            if (!explore.containsKey(sb.toString())) {
                explore.put(sb.toString(), x.initial);
                isComplete(newBoard);
                antri.add(newBoard);
            }
        }
    }

    private void printResult() {
        System.out.println("Initial State:");
        print(this.initial.initial);
        int i = 0;
        while (!this.result.isEmpty()) {
            System.out.println("State " + (i + 1));
            String temp = (String) this.result.pop();
            print(temp);
            i++;
        }
    }

    private void print(String temp) {
        for (int i = 0; i < temp.length(); i++) {
            if (i % 3 == 0 && i != 0) {
                System.out.println("");
                System.out.print(temp.charAt(i) + " ");
            } else {
                System.out.print(temp.charAt(i) + " ");
            }
        }
        System.out.println("");
        System.out.println("");

    }
}
