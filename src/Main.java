
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author yehezkiel
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String initial = "";
        System.out.println("Please enter your initial Board (1-9 tanpa spasi)");
        System.out.println("example:");
        System.out.print("123\n456\n789\n");
        System.out.println("Enter: ");
        String baris1 = sc.next();
        String baris2 = sc.next();
        String baris3 = sc.next();
        
        initial = baris1+baris2+baris3;
        
        System.out.println("Pilih alogritma yang diinginkan \n0: BFS atau 1: A*");
        System.out.print("Pilihan adalah ");
//        String initial = "912345678";

        int pilih = sc.nextInt();

        if (pilih == 0) {
            BFS find = new BFS(initial);
            find.checkFinal();
        } else if (pilih == 1) {
            ABintang bintang = new ABintang(initial);
            bintang.checkFinal();
        }

    }
}
