/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yehezkiel
 */
public class Board implements Comparable<Board>{
    String initial;
    int beban;

    public Board(String initial, int beban) {
        this.initial = initial;
        this.beban = beban;
    }

    @Override
    public int compareTo(Board t) {
        if(this.beban > t.beban)
            return 1;
        else{
            return -1;
        }
    }
    
    
    
}
