
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author yehezkiel
 */
public class BFS {

    Queue<String> antri = new LinkedList();
    HashMap<String, String> explore = new HashMap<String, String>();
    String initial;
    boolean stat = false;
    int z = 1;
    Stack result = new Stack();

    public BFS(String x) {
        this.initial = x;
    }

    public void isComplete(String res) {
        if (res.equals("123456789")) {
            String temp = res;
            while (!temp.equals(this.initial)) {
                result.add(temp);
                temp = explore.get(temp);
            }
            System.out.println("Solution found in " + this.result.size() + " iteration");
            printResult();
            stat = true;
        }
    }

    public void checkFinal() {
        String res = "";
        while (!stat) {
            if (antri.isEmpty()) {
                kiri(initial);
                kanan(initial);
                atas(initial);
                bawah(initial);
            } else {
                res = antri.remove();
                kiri(res);
                kanan(res);
                atas(res);
                bawah(res);
            }
        }
    }

    public void kiri(String x) {
        int y = x.indexOf("9");
        if (y != 0 && y != 3 && y != 6) {
            StringBuilder sb = new StringBuilder(x);
            char temp = x.charAt(y - 1);
            sb.setCharAt(y, temp);
            sb.setCharAt(y - 1, x.charAt(y));
            if (!explore.containsKey(sb.toString())) {
                explore.put(sb.toString(), x);
                isComplete(sb.toString());
                antri.add(sb.toString());
            }
        }

    }

    public void kanan(String x) {
        int y = x.indexOf("9");
        if (y != 2 && y != 5 && y != 8) {
            StringBuilder sb = new StringBuilder(x);
            char temp = x.charAt(y + 1);
            sb.setCharAt(y, temp);
            sb.setCharAt(y + 1, x.charAt(y));
            if (!explore.containsKey(sb.toString())) {
                explore.put(sb.toString(), x);
                isComplete(sb.toString());
                antri.add(sb.toString());
            }
        }
    }

    public void atas(String x) {
        int y = x.indexOf("9");
        if (y > 2) {
            StringBuilder sb = new StringBuilder(x);
            char temp = x.charAt(y - 3);
            sb.setCharAt(y, temp);
            sb.setCharAt(y - 3, x.charAt(y));
            if (!explore.containsKey(sb.toString())) {
                explore.put(sb.toString(), x);
                isComplete(sb.toString());
                antri.add(sb.toString());
            }
        }
    }

    public void bawah(String x) {
        int y = x.indexOf("9");
        if (y < 6) {
            StringBuilder sb = new StringBuilder(x);
            char temp = x.charAt(y + 3);
            sb.setCharAt(y, temp);
            sb.setCharAt(y + 3, x.charAt(y));
            if (!explore.containsKey(sb.toString())) {
                explore.put(sb.toString(), x);
                isComplete(sb.toString());
                antri.add(sb.toString());
            }
        }
    }

    private void printResult() {
        System.out.println("Initial State:");
        print(this.initial);
        int i = 0;
        while(!this.result.isEmpty()) {
            System.out.println("State " + (i+1));
            print((String) this.result.pop());
            i++;
        }
    }

    private void print(String temp) {
        for (int i = 0; i < temp.length(); i++) {
            if (i % 3 == 0 && i != 0) {
                System.out.println("");
                System.out.print(temp.charAt(i) + " ");
            } else {
                System.out.print(temp.charAt(i) + " ");
            }
        }
        System.out.println("");
        System.out.println("");

    }
}
